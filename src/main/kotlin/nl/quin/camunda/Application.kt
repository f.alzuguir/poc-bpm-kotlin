package nl.quin.camunda

import org.springframework.boot.SpringApplication

class Application

fun main(args: Array<String>): Unit {
    SpringApplication.run(Application::class.java)
}

